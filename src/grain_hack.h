/*
    Copyright 2010-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file

    Contains the emitting_grain::calculate_SED_from_intensity
    function. */

#ifndef __grain__
#error grain_hack.h must be included after grain.h
#endif

#ifndef __thermal_equilibrium_grain__
#error grain_hack.h must be included after thermal_equilibrium_grain.h
#endif

/** Calculates the emission SEDs for the cells based on the supplied
    intensity array, cell dust masses and size distribution. This
    function should be virtual, but because we need it to be a
    template to handle the intensity expression template, we
    can't. Instead we have to do the unspeakable and do a manual
    virtual dispatch. Luckily the types are few. */
template<typename T>
void 
mcrx::emitting_grain::
calculate_SED_from_intensity (const blitz::ETBase<T>& intensity,
			      const array_1& mdust,
			      const array_1& dn,
			      array_2 sed, 
			      bool add_to_sed,
			      array_2* temp) const
{
  try {
    const thermal_equilibrium_grain& t = 
      dynamic_cast<const thermal_equilibrium_grain&>(*this);
    t.calculate_SED_from_intensity_virtual(intensity, mdust, dn, 
					   sed, add_to_sed, temp);
  }
  catch (std::bad_cast&) {
    try {
      const template_emission_grain& t =
	dynamic_cast<const template_emission_grain&>(*this);
      t.calculate_SED_from_intensity_virtual(intensity, mdust, dn, 
					     sed, add_to_sed, temp);
    }
    catch (std::bad_cast&) {
      std::cerr << "Unimplemented virtual dispatch hack in emitting_grain::calculate_SED_from_intensity" << std::endl;
      throw;
    }
  }
}
