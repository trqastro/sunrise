/*
    Copyright 2007-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation and explicit instantiations of the
/// aux_particle_stage class. \ingroup mcrx

#include "config.h"
#include "mcrx-stage.h"
#include "mcrx-stage-impl.h"
#include "emergence-fits.h"
#include "create_grid.h"
#include "arepo_grid.h"
#include "arepo_emission.h"
#include "xfer_impl.h"



void mcrx::aux_particle_stage::setup_objects ()
{
  // Build emission
  cout << "Setting up emission" << endl;
  CCfits::ExtHDU& data_hdu = open_HDU(*input_file_, "PARTICLEDATA");
  emi_.reset(new T_emission(data_hdu));

  // Build cameras based on -PARAMETERS HDUs in output file
  cout << "Setting up emergence" << endl;
  eme_.reset(new T_emergence (*output_file_));

  // no dust model or grid is used
}


void mcrx::aux_particle_stage::load_file ()
{
  // first set up general stuff
  setup_objects();

  // Only thing to load is camera images
  this->eme_->load_images(*this->output_file_);
}


void mcrx::aux_particle_stage::load_dump (binifstream& dump_file)
{
  // first set up general stuff
  setup_objects();

  // Only thing to load is camera images
  eme_->load_dump(dump_file);
}


void mcrx::aux_particle_stage::save_file ()
{
  // Get normalization and save images
  const T_float normalization = emi_->total_emission_weight();
  eme_->write_images(*output_file_, normalization, m_.units);
}


void mcrx::aux_particle_stage::save_dump (binofstream& dump_file) const
{
  eme_->write_dump(dump_file);
}


bool mcrx::aux_particle_stage::shoot ()
{
  return shoot_nonscatter ();
}

void mcrx::aux_particle_stage::operator() ()
{
  // this is here so the function is instantiated
  run_stage();
}

// prevent instantiation of the xfer template in each stage. instead
// we do this in one place only
/*
extern template class mcrx::xfer<mcrx::aux_particle_stage::T_dust_model,
				 mcrx::dummy_grid, 
				 mcrx::mcrx_rng_policy>;
*/
// no explicit instantiations needed here since this is not a template
