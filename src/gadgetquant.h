/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file 

    Declaration of the Gadgetheader class. */

#ifndef __gadgetquant__
#define __gadgetquant__

#include <vector>

namespace mcrx {
  class Gadgetheader;
};

/** The Gadgetheader class represents the data in a Gadget type 1 file
    header. This file format is very fragile and its use is
    discouraged. */
class mcrx::Gadgetheader
{
public:
  void loadfrheader(const char snapfn[255]);
  void printheader() const;
  bool is_springel_file;
  std::vector<int>      npart;
  std::vector<double>   mass;
  double   time;
  double   redshift;
  int      flag_sfr;
  int      flag_feedback;
  std::vector<int>      npartTotal;
  int      flag_cooling;
  int      num_files;
  double   BoxSize;
  double   Omega0;
  double   OmegaLambda;
  double   HubbleParam;
  int      flag_multiphase;
  int      flag_stellarage;
  int      flag_sfrhistogram;
  int      flag_stargens;
  int      flag_snapaspot; // supposed to be SnapHasPot, has potential
  int      flag_metals;
  int      flag_energydetail;
  int flag_parentID;
  int flag_starorig;

  Gadgetheader() : npart(6), mass(6), npartTotal(6) {};
};

  
#endif
