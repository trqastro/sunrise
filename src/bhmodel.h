/*
    Copyright 2008-2011 Patrik Jonsson, sunrise@familjenjonsson.org.

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Declaration of the black hole model class.
/// \ingroup sfrhist

// $Id$

#ifndef __bhmodel__
#define  __bhmodel__

#include <vector>
#include <string> 

#include "blitz/array.h"
#include "particle_set.h"
#include "vecops.h"
#include "mcrx-units.h"

namespace mcrx {
  class bhmodel;
}

class Preferences;
class sfrhist;
namespace CCfits {
  class FITS;
};

/** Black hole SED model. This class functions very much like the
    stellarmodel class, except that the SED is indexed by bolometric
    luminosity instead of age and metallicity. The BH SED is read from
    a FITS file. */
class mcrx::bhmodel {
private:
  Preferences& prefs;
  std::string file_name; ///< The black hole model filename.

  /// The array holding the SEDs. Black hole bolometric luminosity
  /// is first index and lambda is second.
  array_2 sed;
  /// The luminosities of entries in the SED array.
  std::vector<T_float> log_L_bol; 
  /// The luminosity midway between SED entries.
  std::vector<T_float> middle_log_L_bol; 
  /// The wavelengths in the SED array.
  std::vector<T_float> lambdas; 
  /// Array referencing the vector wavelength data
  array_1 alambdas;

  /// The radiative efficiency of accretion, determines the conversion
  /// from accretion rate to bolometric luminosity.
  T_float mdot2lum_;

  // some useful parameters
  bool log_flux; ///< If true, the SED array contains log10 of the luminosity.
  T_unit_map units_;

public:
  ~bhmodel() {};
  bhmodel(Preferences& p);

  // Writes BHMODEL HDU from the input file to the output file
  void write_fits_parameters (const std::string&) const;

  array_1 get_SED(bh_particle_set&, int index) const;

  /*** Resamples the SED in the stellar model onto the wavelength
       vector supplied. Normally this just involves interpolating, but
       the class is free to do something more advanced like actually
       integrate to make sure energy is preserved. */
  void resample(const array_1& lambda);

  /** Returns the wavelength interval over which each of the points in
      the SED is valid.  This is handy when integrating over
      wavelength. */
  const array_1 delta_lambda() const {
    return delta_quantity(lambdas);};

  /** Returns the wavelength array of the SEDs.  NOTE that the returned
      array actually references the internal wavelength vector, so it's
      only valid as long as the model object exists. */
  const array_1 lambda() const {
    return alambdas;};

  int n_lum() const {return sed.extent(blitz::firstDim);};
  int n_lambda() const {return sed.extent(blitz::secondDim);};

  /// Returns units map.
  const T_unit_map& units () const {return units_;};
};


#endif
  
